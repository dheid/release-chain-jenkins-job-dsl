def releaseJobNamePrefix = "release"
def releaseIncludes = 'some.group.id:some.artifact.name' // see http://www.mojohaus.org/versions-maven-plugin/examples/use-releases.html

def artifacts = [
        "artifact_a",
        "artifact_b"
]

artifacts.each { artifactName ->
    job("${releaseJobNamePrefix}-${artifactName}") {
        scm {
            git("git-repository-url", 'master')
        }
        steps {
            maven {
                goals("versions:use-releases -Dincludes=${releaseIncludes}")
            }
            maven {
                goals('scm:checkin')
            }
            maven {
                goals('-B release:prepare')
            }
            maven {
                goals('release:perform')
            }
        }
    }
}

multiJob('release_chain') {

    parameters {
        artifacts.each { artifactName ->
            booleanParam("release_${artifactName}", false, "")
        }
    }

    steps {
        phase('Update dependency versions and release') {
            executionType('SEQUENTIALLY')
            artifacts.each { artifactName ->
                phaseJob("${releaseJobNamePrefix}-${artifactName}") {
                    abortAllJobs(true)
                    configure { phaseJobConfig ->
                        phaseJobConfig / enableCondition << 'true'
                        phaseJobConfig / condition << "\${release_${artifactName}}"
                    }
                }
            }
        }
    }
}