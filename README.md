# release-chain-jenkins-job-dsl

A Jenkins Job DSL script to create a chain of release jobs for Maven artifacts

This repo uses slighty adapted code from the following repositories:

* https://github.com/sheehan/job-dsl-gradle-example
* https://github.com/marcelbirkner/docker-jenkins-job-dsl

## Testing

```
./gradlew test
```

## Building the Docker image

```
docker build -t release-chain docker
docker run -p 8080:8080 -v `pwd`/jenkins_jobs:/var/jenkins_home/jobs release-chain
```